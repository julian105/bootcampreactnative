console.log("Soal No 1")

function teriak() {
    return "Hallo Sanbers!"
}

console.log(teriak()) // "Halo Sanbers!"


console.log("Soal No 2")

function kalikan() {
    return num1 * num2
}
var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

console.log("Soal No 3")

function introduce(name, age, address, hobby) {
    return ("Nama Saya" + " " + name + " " + "Umur saya" + " " + age + "Tahun" + " " + "Alamat saya di" + " " + address + " " + "Dan saya punya hobby yaitu" + " " + hobby)
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

console.log(introduce(name, age, address, hobby)) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"