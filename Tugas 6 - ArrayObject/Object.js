var dataArray = [
    ["Abduh", "Muhammad", "male", 1992],
    ["Ahmad", "Taufik", "male", 1985]

]

function arrayToObject(dataArray) {
    var now = new Date()
    var thisYear = now.getFullYear()

    for (var a = 0; a < dataArray.length; a++) {
        if (!dataArray[a][3] || dataArray[a][3] > thisYear) {
            age = "Invalid Birth Year"
        } else {
            age = thisYear - dataArray[a][3]
        }

        var obj = {
            firstname: dataArray[a][0],
            lastName: dataArray[a][1],
            gender: dataArray[a][2],
            age: age
        }
        console.log(obj.firstname) + (obj.lastName + ' :')
        console.log(obj)
    }
}
arrayToObject(dataArray)