console.log("Soal No 1")

goldenFunction = () => {
    console.log("this is golden!!")
}
goldenFunction()

console.log("Soal No 2")

const firstName = "William"
const lastName = "Imoh"

const nama = `${firstName} ${lastName}`

console.log(nama)

console.log("Soal No 3")

let drivercode = {
    firstName1: "Harry",
    lastName1: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
};

const { firstName1, lastName1, destination, occupation } = drivercode;
console.log(firstName1, lastName1, destination, occupation)

console.log("Soal No 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];
//Driver Code
console.log(combined);

console.log("Soal No 5")

const planet = "earth"
const view = "glass"

const before = 'Lorem ' + `${view}` + 'dolor sit amet, ' +
    'consectetur adipiscing elit,' + `${planet}` + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'

console.log(before)