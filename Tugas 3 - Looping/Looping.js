console.log("Looping While")
var jumlah = 0;
console.log("Looping Pertama");
while (jumlah < 20) {
    jumlah += 2;

    console.log(jumlah + " - I Love Coding");
}

console.log("Looping Kedua");
while (jumlah > 2) {
    console.log(jumlah + " - I Will became a mobile developer");
    jumlah -= 2;
}

console.log(jumlah);

console.log("Looping for")
for (var i = 1; i <= 20; i++) {
    if (i % 2 == 0) {
        console.log(i + "-Berkualitas");
    } else if (i % 2 !== 0) {
        if (i % 3 == 0) {
            console.log(i + "-I Love Coding");
        } else {
            console.log(i + "-Santai");
        }
    }
}

console.log("Membuat Persegi Panjang")
for (var i = 1; i <= 4; i++) {
    console.log("########");
}

console.log("Membuat Tangga")
for (var i = 1; i <= 7; i++) {
    var str = " ";
    for (var j = 1; j <= i; j++) {
        str += "#"
    }
    console.log(str);
}