console.log("Soal No 1")

function range(startNum, finishNum) {
    var rangeArr = [];

    if (startNum > finishNum) {
        var rangeLength = startNum - finishNum + 1;
        for (var a = 0; a < rangeLength; a++) {
            rangeArr.push(startNum - a)
        }
    } else if (startNum < finishNum) {
        var rangeLength = finishNum - startNum + 1;
        for (var a = 0; a < rangeLength; a++) {
            rangeArr.push(startNum + a)
        }
    } else if (!startNum || !finishNum) {
        return -1
    }
    return rangeArr
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    //console.log(range(1)) // -1
    //console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
    // console.log(range(54, 50)) // [54, 53, 52, 51, 50]
    // console.log(range()) // -1


console.log("Soal No 2")

function rangeWithStep(startNum, finishNum, step) {
    var angka = [];

    if (startNum > finishNum) {
        var currentNum = startNum;
        for (var i = 0; currentNum >= finishNum; i++) {
            angka.push(currentNum)
            currentNum -= step
        }
    } else if (startNum < finishNum) {
        var currentNum = startNum;
        for (var i = 0; currentNum <= finishNum; i++) {
            angka.push(currentNum)
            currentNum += step
        }
    } else if (!startNum || !finishNum || step) {
        return -1
    }
    return angka
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
    //console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
    //console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
    //console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log("Soal No 4")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(data) {
    var dataLength = data.length
    for (var i = 0; i < dataLength; i++) {
        var id = "Nomor id : " + data[i][0]
        var nama = "Nama Lengkap : " + data[i][1]
        var ttl = "TTL: " + data[i][2] + " " + data[i][3]
        var hobi = "Hobi : " + data

        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)
    }

}
dataHandling(input)


console.log("Soal No 5")

function balikKata(kata) {
    var KataBaru = " ";
    for (var a = kata.length - 1; a >= 0; a--) {
        KataBaru += kata[a]
    }
    return KataBaru;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I