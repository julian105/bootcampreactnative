import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class videoitem extends Component {
    render() {
        let video = this.props.video;
        return ( <
            View style = { styles.container } >
            <
            Image source = {
                { uri: video.snippet.thumbnails.medium.url }
            }
            style = {
                { height: 200 }
            }
            /> <
            View style = { styles.descContainer } >
            <
            Image source = {
                { uri: 'https://randomuser.me/api/portraits/men/1.jpg' }
            }
            style = {
                { width: 50, height: 50 }
            }
            /> < /
            View > <
            /View >

        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 15
    },
    descContainer: {
        flexDirection: 'row'
    }
});